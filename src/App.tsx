//imports from inbuilt libraries
import React, { Fragment } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

// imports from components and pages
import Navbar from "./components/Navbar/Navbar";
import Login from "./components/Login";
import Landing from "./components/Landing";
import { Register } from "./components/Register";

import { ErrorPage } from "./pages/ErrorPage";




const App = () => {
  return (
    <Router>
      <Fragment>
        <Navbar />
        <Routes>
          <Route path="/" element={<Landing />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/*" element={<ErrorPage />} />
        </Routes>
      </Fragment>
    </Router>
  );
};

export default App;
