import React from "react";
import { Container, TextField } from "@mui/material";
import Date from "./Utils/Date";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

const Landing = () => {
  return (
    <Container>
      <Box sx={{ xflexGrow: 1 }}>
        <Grid container spacing={3}>
          <Grid item xs>
            <TextField id="outlined-basic" label="From" variant="outlined" />
          </Grid>
          <Grid item xs>
            <TextField id="outlined-basic" label="To" variant="outlined" />
          </Grid>
          <Grid item xs>
            <Date />
          </Grid>
          <Grid item xs>
            <Button variant="contained">Search Buses</Button>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
};

export default Landing;
