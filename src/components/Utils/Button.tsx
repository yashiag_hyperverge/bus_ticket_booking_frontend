import * as React from 'react';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';

export function ContainedButtons() {
  return (
    <Stack direction="row" spacing={2}>
      <Button variant="contained">Contained</Button>
    </Stack>
  );
}

export function DisabledButton() {
    return (
      <Stack direction="row" spacing={2}>
        <Button variant="contained" disabled>
          Disabled
        </Button>
        
      </Stack>
    );
  }

 
